#!/usr/bin/env python
from __future__ import print_function
import pygame
import sys
import os
from settings import *

pygame.init()


class Camera:
	def __init__(self, width, height):
		self.rect = pygame.Rect(0, 0, width, height)
		self.width = width
		self.height = height

	#def apply(self, entity):
	#	print('apply')
	#	print(entity.x, entity.y, self.camera.x, self.camera.y)
	#	entity.x = self.camera.x
	#	entity.y = self.camera.y

	def update(self, target):
		x = -target.x + int(SCREEN_WIDTH / 2)
		y = -target.y + int(SCREEN_HEIGHT / 2)
		
		x = min(0, x)
		y = min(0, y)
		x = max(-(self.width - SCREEN_WIDTH), x)
		y = max(-(self.height - SCREEN_HEIGHT), y)
				
		self.rect = pygame.Rect(x, y, self.width, self.height)
		print('Camera update:')
		print(self.rect)


class Player(object):
	def __init__(self):
		self.x = 0
		self.y = 0
		self.height = TILE_SIZE_PX
		self.width = TILE_SIZE_PX
		self.color = GREY
		self.speed = 4

	def move_left(self):
		if self.x > 0:
			self.x -= self.speed

	def move_right(self):
		if self.x < MAP_WIDTH_PX:
			self.x += self.speed

	def move_up(self):
		if self.y > 0:
			self.y -= self.speed

	def move_down(self):
		if self.y < MAP_HEIGHT_PX:
			self.y += self.speed

	def jump(self):
		self.y -= self.speed

	def update(self):
		pass
		
	def draw(self, screen):
		pygame.draw.rect(screen, self.color, (self.x, self.y, self.height, self.width), 0)


class Map(object):
	def __init__(self, width, height):
		self.width = width
		self.height = height
		self.x = TILE_SIZE_PX
		self.y = TILE_SIZE_PX
		self.tiles = self.create_tiles()
		
	def create_tiles(self):
		tileset = []
		for tile_y in range(0, self.height):
			line = []
			tileset.append(line)
			y = (tile_y * TILE_SIZE_PX) + self.y
			for tile_x in range(0, self.width):
				x = (tile_x * TILE_SIZE_PX) + self.x
				line.append(Tile(x, y))
		return tileset

	def update(self, screen, x, y):
		self.x = x
		self.y = y
		for tile_y in range(0, self.height):
			y = (tile_y * TILE_SIZE_PX) + self.y
			for tile_x in range(0, self.width):
				x = (tile_x * TILE_SIZE_PX) + self.x
				self.tiles[tile_y][tile_x].x = x
				self.tiles[tile_y][tile_x].y = y

	def draw(self, screen):
		for i in self.tiles:
			for j in i: 
				j.draw(screen)
				

class Tile(object):
	def __init__(self, x, y):
		self.img = pygame.image.load(os.path.join('img','testtile32.gif'))
		self.width = TILE_SIZE_PX
		self.height = TILE_SIZE_PX
		self.x = x
		self.y = y

	def update(self, screen, x, y):
		self.x = x
		self.y = y

	def draw(self, screen):
		screen.blit(self.img, (self.x, self.y))


class Game(object):
	def __init__(self):
		self.game_over = False
		self.map = Map(MAP_WIDTH_TILE, MAP_HEIGHT_TILE)
		self.player = Player()
		self.camera = Camera(MAP_WIDTH_PX, MAP_HEIGHT_PX)

	def process_events(self):
		keys = pygame.key.get_pressed()

		if keys[pygame.K_LEFT]:
			self.player.move_left()
		if keys[pygame.K_RIGHT]:
			self.player.move_right()
		if keys[pygame.K_UP]:
			self.player.move_up()
		if keys[pygame.K_DOWN]:
			self.player.move_down()
					
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()

		print('Player Move:')
		print('Player.x = ', str(self.player.x))
		print('Player.y = ', str(self.player.y))
		
	def display_frame(self, screen):
		screen.fill(DARKGREY)

	def update_objects(self, screen):
		self.player.update()
		self.camera.update(self.player)
		self.map.update(screen, self.camera.rect.x, self.camera.rect.y)
		
	def draw_objects(self, screen):
		self.map.draw(screen)
		self.player.draw(screen)
		

def main():
	pygame.init()
	
	screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
	clock = pygame.time.Clock()
	
	game = Game()

	while not game.game_over:
		game.process_events()
		game.display_frame(screen)
		game.update_objects(screen)
		game.draw_objects(screen)
		
		pygame.display.update()
		clock.tick(30)


if __name__ == '__main__':
	main()
